module type SeqLike = sig

  (** Immutable 1 dimensional sequence of data, such as

      * lists
      * arrays
      * string
      * streams
  *)

  type 'a t

  (** {1 Construction} *)
    
  val empty : 'a t
  (** Length 0 *)
    
  val singleton : 'a -> 'a t
  (** Build a [t] of length 1 *)

  val make : int -> 'a -> 'a t
  (** [make n e] builds a [t] of length [n] filled with [e] *)
    
  val init : int -> (int -> 'a) -> 'a t
  (** [init f e] builds a [t] of length [n] filled with [f i] for each i-th 'aent *)

  (** {1 Length} *)

  val length : 'a t -> int
  (** The order is dependent on the implementation. 
      It may not terminate if it is an infinite stream *)

  (** {1 Conversion} *)
    
  val to_list : 'a t -> 'a list
  val to_array : 'a t -> 'a array

  val of_list : 'a list -> 'a t
  val of_array : 'a array -> 'a t

  (** {1 Element access} *)

  val get_opt : 'a t -> int -> 'a option
  (** The order is dependent on the implementation. *)

  val get : 'a t -> int -> 'a
  (** The order is dependent on the implementation. It may raise an exception. *)

  (** {1 Basics} *)

  val append : 'a t -> 'a t -> 'a t

  val concat : 'a t t -> 'a t

  val sub : 'a t -> int -> int -> 'a t

  val rev : 'a t -> 'a t

  val replicate : 'a t -> int -> 'a t

  (** {1 Functionals} *)

  val iter : ('a -> unit) -> 'a t -> unit

  val map : ('a -> 'b) -> 'a t -> 'b t

  val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

  val fold_left : ('st -> 'a -> 'st) -> 'st -> 'a t -> 'st
  val fold_right : ('a -> 'st -> 'st) -> 'a t -> 'st -> 'st

  val fold_left1 : ('a -> 'a -> 'a) -> 'a t -> 'a (** fail when empty *)
  val fold_right1 : ('a -> 'a -> 'a) -> 'a t -> 'a (** fail when empty *)

  val map_accum_left : ('acc -> 'a -> ('acc * 'b)) -> 'acc -> 'a t -> ('acc * 'b list)

  val scanl : ('acc -> 'a -> 'acc) -> 'acc -> 'a t -> 'acc t
  val scanr : ('a -> 'acc -> 'acc) -> 'a t -> 'acc -> 'acc t

  val scanl1 : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
  val scanr1 : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
    
  val iteri : (int -> 'a -> unit) -> 'a t -> unit
  val mapi : (int -> 'a -> 'b) -> 'a t -> 'b t
  val map2i : (int -> 'a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
  val filter_mapi : (int -> 'a -> 'b option) -> 'a t -> 'b t

  val loop : ('acc -> 'a -> [> `Continue of 'acc | `Stop of 'acc]) -> 'acc -> 'a t -> 'acc
  val loop_right : ('acc -> 'a -> [> `Continue of 'acc | `Stop of 'acc]) -> 'acc -> 'a t -> 'acc
    
  (** {1 Membership} *)

  val mem : 'a t -> 'a -> bool

  val index : 'a t -> 'a -> int option

  val rindex : 'a t -> 'a -> int option
    
  val find : 'a t -> ('a -> bool) -> 'a option

  val find_right : 'a t -> ('a -> bool) -> 'a option

  val findi : 'a t -> (int -> 'a -> bool) -> (int * 'a) option
  val findi_right : 'a t -> (int -> 'a -> bool) -> (int * 'a) option

  val filter : 'a t -> ('a -> bool) -> 'a t

  val concat_map : ('a -> 'b t) -> 'a t -> 'b t
  val filter_map : ('a -> 'b option) -> 'a t -> 'b t

  val partition : 'a t -> ('a -> bool) -> 'a t * 'a t

  val assoc : 'a -> ('a * 'b) t -> 'b option

  val assq : 'a -> ('a * 'b) t -> 'b option

  val mem_assoc : 'a -> ('a * 'b) t -> bool
    
  val mem_assq : 'a -> ('a * 'b) t -> bool
    
  (** {1} Takes and drops *)

  val take_while : ('a -> bool) -> 'a t -> 'a t

  val drop_while : ('a -> bool) -> 'a t -> 'a t

  val take : int -> 'a t -> 'a t

  val drop : int -> 'a t -> 'a t
    
  (** {1} Sort *)
    
  val sort : ('a -> 'a -> int) -> 'a t -> 'a t

  val sort_uniq : ('a -> 'a -> int) -> 'a t -> 'a t

  val group : ('a -> 'a -> int) -> 'a t -> 'a t t

  (** {1} Comparison *)

  val for_all : ('a -> bool) -> 'a t -> bool

  val for_all2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool

  val exists : ('a -> bool) -> 'a t -> bool

  val exists2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool

  (** {1 List of pairs} *)

  val zip : 'a t -> 'b t -> ('a * 'b) t

  val unzip : ('a * 'b) t -> 'a list * 'b list
    
end

module List : SeqLike with type 'a t = 'a list = struct

  type 'a t = 'a list

  let empty = []
    
  let singleton x = [x]

  let make n x =
    if n < 0 then invalid_arg "List.make";
    let rec loop st = function
      | 0 -> st
      | n -> loop (x::st) (n-1)
    in
    loop [] n
      
  let init n f =
    if n < 0 then invalid_arg "List.init";
    let rec loop st = function
      | -1 -> st
      | i -> loop (f i :: st) (i-1)
    in
    loop [] (n-1)

  let init n f =
    if n < 0 then invalid_arg "List.init";
    let rec loop st = function
      | -1 -> st
      | i -> loop (f i :: st) (i-1)
    in
    loop [] (n-1)

  let length = List.length

  let to_list t = t

  let to_array = Array.of_list

  let of_list t = t

  let of_array = Array.to_list
    
  let get_opt t n =
    if n < 0 then invalid_arg "List.get_opt";
    let rec loop n = function
      | [] -> None
      | x::_ when n = 0 -> Some x
      | _::xs -> loop (n-1) xs
    in
    loop n t

  let get t n = match get_opt t n with
    | Some x -> x
    | None -> failwith "List.get"

  let nth = get

  let rev t =
    let rec loop st = function
      | [] -> st
      | x::xs -> loop (x::st) xs
    in
    loop [] t

  let rec rev_append t1 t2 =
    match t1 with
    | [] -> t2
    | x::xs -> rev_append xs (x :: t2) 

  let append t1 t2 = rev_append (rev t1) t2

  let append_ntr = (@)

  let rec fold_left f acc = function
    | [] -> acc
    | x::xs -> fold_left f (f acc x) xs
    
  let fold_right f xs acc = fold_left f acc @@ rev xs

  let rec fold_right_ntr f xs acc =
    match xs with
    | [] -> acc
    | x::xs -> f x (fold_right_ntr f xs acc) 

  let fold_left1 f = function
    | [] -> invalid_arg "List.fold_left1"
    | x::xs -> fold_left f x xs
      
  let fold_right1 f xs = fold_left1 f @@ rev xs
      
  let concat = function
    | [] -> []
    | ts -> fold_left1 append ts

  let take_strict len xs =
    if len < 0 then invalid_arg "List.take_strict";
    let rec loop st len xs =
      if len = 0 then rev st
      else match xs with
      | [] -> invalid_arg "List.take_strict"
      | x::xs -> loop (x::st) (len-1) xs
    in
    loop [] len xs

  let drop_strict len xs =
    if len < 0 then invalid_arg "List.drop_strict";
    let rec loop len xs =
      if len = 0 then xs
      else match xs with
      | [] -> invalid_arg "List.drop_strict"
      | x::xs -> loop (len-1) xs
    in
    loop len xs

  let sub xs pos len = take_strict len @@ drop_strict pos xs

  let replicate xs n =
    if n < 0 then invalid_arg "List.replicate";
    match n with
    | 0 -> []
    | 1 -> xs
    | n ->
        let xs' = rev xs in
        let rec loop st = function
          | 0 -> st
          | n -> loop (rev_append xs' st) (n-1)
        in
        loop [] n

  let rec iter f = function
    | [] -> ()
    | x::xs -> f x; iter f xs
      
(*


  val iter : ('a -> unit) -> 'a t -> unit

  val map : ('a -> 'b) -> 'a t -> 'b t

  val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

  val fold_left : ('st -> 'a -> 'st) -> 'st -> 'a t -> 'st
  val fold_right : ('a -> 'st -> 'st) -> 'a t -> 'st -> 'st

  val fold_left1 : ('a -> 'a -> 'a) -> 'a t -> 'a (** fail when empty *)
  val fold_right1 : ('a -> 'a -> 'a) -> 'a t -> 'a (** fail when empty *)

  val map_accum_left : ('acc -> 'a -> ('acc * 'b)) -> 'acc -> 'a t -> ('acc * 'b list)

  val scanl : ('acc -> 'a -> 'acc) -> 'acc -> 'a t -> 'acc t
  val scanr : ('a -> 'acc -> 'acc) -> 'a t -> 'acc -> 'acc t

  val scanl1 : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
  val scanr1 : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
    
  val iteri : (int -> 'a -> unit) -> 'a t -> unit
  val mapi : (int -> 'a -> 'b) -> 'a t -> 'b t
  val map2i : (int -> 'a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
  val filter_mapi : (int -> 'a -> 'b option) -> 'a t -> 'b t

  val loop : ('acc -> 'a -> [> `Continue of 'acc | `Stop of 'acc]) -> 'acc -> 'a t -> 'acc
  val loop_right : ('acc -> 'a -> [> `Continue of 'acc | `Stop of 'acc]) -> 'acc -> 'a t -> 'acc
    
  (** {1 Membership} *)

  val mem : 'a t -> 'a -> bool

  val index : 'a t -> 'a -> int option

  val rindex : 'a t -> 'a -> int option
    
  val find : 'a t -> ('a -> bool) -> 'a option

  val find_right : 'a t -> ('a -> bool) -> 'a option

  val findi : 'a t -> (int -> 'a -> bool) -> (int * 'a) option
  val findi_right : 'a t -> (int -> 'a -> bool) -> (int * 'a) option

  val filter : 'a t -> ('a -> bool) -> 'a t

  val concat_map : ('a -> 'b t) -> 'a t -> 'b t
  val filter_map : ('a -> 'b option) -> 'a t -> 'b t

  val partition : 'a t -> ('a -> bool) -> 'a t * 'a t

  val assoc : 'a -> ('a * 'b) t -> 'b option

  val assq : 'a -> ('a * 'b) t -> 'b option

  val mem_assoc : 'a -> ('a * 'b) t -> bool
    
  val mem_assq : 'a -> ('a * 'b) t -> bool
    
  (** {1} Takes and drops *)

  val take_while : ('a -> bool) -> 'a t -> 'a t

  val drop_while : ('a -> bool) -> 'a t -> 'a t

  val take : int -> 'a t -> 'a t

  val drop : int -> 'a t -> 'a t
    
  (** {1} Sort *)
    
  val sort : ('a -> 'a -> int) -> 'a t -> 'a t

  val sort_uniq : ('a -> 'a -> int) -> 'a t -> 'a t

  val group : ('a -> 'a -> int) -> 'a t -> 'a t t

  (** {1} Comparison *)

  val for_all : ('a -> bool) -> 'a t -> bool

  val for_all2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool

  val exists : ('a -> bool) -> 'a t -> bool

  val exists2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool

  (** {1 List of pairs} *)

  val zip : 'a t -> 'b t -> ('a * 'b) t

  val unzip : ('a * 'b) t -> 'a list * 'b list
    
*)    
end
  
